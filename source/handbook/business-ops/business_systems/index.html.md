---
layout: markdown_page
title: "Business Systems"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## [Business Systems Analyst](/job-families/finance/business-system-analyst/)

### Current High Level Projects:
*  Streamline Onboarding
*  User Journey Mapping
*  Portal Improvements


## [Business Systems Specialist](/job-families/finance/business-system-analyst/)

### Incoming Customer Inquiry Matrix: Licensing, Billing, Transactions

| Common Request | Responsible Group | Reference Systems | Workflow Link    | Comments |
|------|:-------:|:-------:|:----------:|:-------:|
| User reports an inability to upgrade from one paid plan to another | SMB | SFDC [Customers portal](https://customers.gitlab.com/customers/sign_in)     |  -    |  - |
| User is on a trial and wants to purchase a paid plan   | SMB | SFDC [Customers portal](https://customers.gitlab.com/customers/sign_in)      |   -   |  - |
| User wants to trial a plan other than Gold on GitLab.com | .com Support    |   [Internal Issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=plan_change_request)      |  [Workflow](/handbook/support/internal-support/#common-internal-requests---sales-team--technical-account-managers)    | -  |
| User wants to extend GitLab.com trial | .com Support    |   [Internal Issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=plan_change_request)      |  [Workflow](/handbook/support/internal-support/#common-internal-requests---sales-team--technical-account-managers)    | -  |
| User receives an error during the purchasing process within the customers portal   |    engineering   |  -  |  -   | -  |
| User wants to downgrade subscription |  AR     | SFDC [Customers portal](https://customers.gitlab.com/customers/sign_in)    | - |  - |
| User wants the red renewal approaching banner message in their Self-Managed system removed | bizops      |- | - |  - |
| User doesn't know the steps to purchase a GitLab.com subscription | bizops or .com Support |  -| - | - |
| User doesn't see their group during purchase process | bizops or .com Support | - | - | - |


### License Requests

| Common Request | Responsible Group | Systems Used | Workflow Link    | Comments |
|------|:-------:|:-------:|:----------:|:-------:|
| User doesn't understand how true-up works |  SMB      |  -   |  -  | -  |
| Instructions for activating the license key   |   bizops    |   -  |   -  |  - |
| User wants to know when they will receive the license key | bizops    |   -   |  -  |  - |
| User doesn't renew paid Self-Managed plan, what happens to the license and features   |   SMB    |   -|  -  | - |
| A customer reports problems when registering their license key |    bizops   | - |  -| -  |


### Billing Requests

| Common Request | Responsible Group | Systems Used | Workflow Link    | Comments |
|------|:-------:|:-------:|:----------:|:-------:|
| Copy of invoice |    AR   |  - | - | -  |
| Changes to invoice (address, company name, VAT #, etc)   |    AR   |  - | - |  - |
| Refund requests  | AR    | - | -|  - |
| Requests to make a payment/payment failed   |   AR   | - | - | -  |
